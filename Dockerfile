FROM ubuntu:16.04
MAINTAINER Foucault de Bonneval <foucault(at)commit.ninja>

ADD resources /var/tmp/resources
RUN /var/tmp/resources/build.sh

EXPOSE 6343/udp 5678
CMD ["bash"]
