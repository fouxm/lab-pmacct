#!/bin/bash

set -xe

# Install dependencies
apt-get update
apt-get install -y \
  autoconf \
  cmake \
  git \
  libmysqlclient-dev \
  libpcap-dev \
  libtool \
  pkg-config 

# Get source and build
git clone https://github.com/pmacct/pmacct
cd pmacct
./autogen.sh
./configure --enable-64bit --enable-mysql --enable-threads
make -j4

