VERSION    ?= master
REPOSITORY ?= pmacct
IMAGE      ?= $(REPOSITORY):$(VERSION)

BUILD_OPTIONS = -t $(IMAGE)
ifdef http_proxy
BUILD_OPTIONS += --build-arg http_proxy=$(http_proxy)
endif
ifdef https_proxy
BUILD_OPTIONS += --build-arg https_proxy=$(https_proxy)
endif
BUILD_OPTIONS += --build-arg application_version=$(VERSION)


build:
	docker build $(BUILD_OPTIONS) .


